let knap;
let mic;
let ctracker;
let capture;

function setup(){
    createCanvas(500,500);
    background(250, 250, 150);
    knap = createButton("Tryk på mig!");
    knap.position(50,50);  
    knap.size(100,50);
    knap.style("background-color","#00aaaa");
    knap.style("color", "#ffffff"); 

    knap.mousePressed(skifteFarve);
    knap.mouseOut(resetFarve);

    mic = new p5.AudioIn();
    mic.start();

    capture = createCapture(VIDEO);
    capture.size(640, 480);
    capture.hide();

    ctracker = new clm.tracker();
    ctracker.init(pModel);
    ctracker.start(capture.elt);
}
function draw(){
    background(250, 250, 150);
    let volume = mic.getLevel();
    let mappedVolume = map(volume, 0, 1, 0, 1000);
    
    
    knap.size(mappedVolume);

    image(capture, 0, 0, 640, 480);
    filter(INVERT);

    let positioner = ctracker.getCurrentPosition();

    if(positioner.length){

        knap.position(positioner[33][0], positioner[33][1]);

        for(i=0; i < positioner.length; i++){
            ellipse(positioner[i][0], positioner[i][1], 5);
        }

    }
    ellipse(width/2, height/2, mappedVolume);

}

function keyPressed(){
    if (keyCode === 32){
        knap.style("transform", "rotate(45deg)");
    } else {
        knap.style("transform", "rotate(0deg)");
    }

}

function skifteFarve(){
    knap.style("background-color", "#ff0000");
    userStartAudio();
}

function resetFarve(){
    knap.style("background-color", "#00aaaa");
}