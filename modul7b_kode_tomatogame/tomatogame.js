let tomato = [];
let player;

let antal = 15;

function setup() {
    createCanvas(500, 500);
    background(240);
    for (let i = 0; i < antal; i++) {
        tomato.push(new Tomato());
    }

    player = new Player();
 
  

}

function draw() {
    background(240);
    spawnTomato();
    player.show();
    checkCollision();
}

function spawnTomato() {
    for (let i = 0; i < tomato.length; i++) {
        tomato[i].move();
        tomato[i].show();
    }
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        player.moveUp();
    }
    if (keyCode === DOWN_ARROW) {
        player.moveDown();
    }
    if (keyCode === ENTER){
        setup();
        spawnTomato();
        player.show();
        checkCollision();
    }
}

function checkCollision() {
    let distance;
    for (let i = 0; i < tomato.length; i++) {
        distance = dist(player.posX, player.posY, tomato[i].posX, tomato[i].posY);

        if (distance < 50-player.size/2) {
            noLoop();
            text("game over", width / 2, height / 2);
            console.log("detected");
        }
    }

}


