class Tomato {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, height-80);
        this.speed = random(0.2, 2);
    }

    move(){
        this.posX += this.speed;
        if (this.posX > width){
            this.posX = 0;
        }
    }

    show() {
        fill(255, 0, 0);
        noStroke();
        ellipse(this.posX, this.posY, 50, 40);
        fill(0, 255, 0);
        ellipse(this.posX, this.posY - 25, 5, 20);
    }
}

