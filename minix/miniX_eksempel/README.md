# Et eksempel på en miniX
*OBS: Dette er et eksempel på indholdet på en miniX aflevering*

Her er et screenshot af mit program:

![](/minix/miniX_eksempel/screenshot.png)

Og her er et link til at køre mit program:

https://margretelr.gitlab.io/aestetisk-programmering-2023/minix/miniX_eksempel/eksempel-sketch/index.html

Link til selve source koden findes her: 

https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/blob/main/minix/miniX_eksempel/eksempel-sketch/sketch.js

# Beskrivelse af programmet
Mit program tegner nogle farverige firekanter med random skiftende længder ovenpå hinanden. Når længderne lander over et bestemt antal, så beder jeg programmet tegne de næste firekanter længere nede. Og sådan bliver den ved indtil den når udover canvaset, hvor den bliver bedt om at starte ved toppen igen. På den måde opnår jeg et billede skiftende billede af flotte firekanter.

Programmet udnytter `draw()` funktionens loopende egenskab, til at skabe en animations-agtigt effekt. Derudover bruges `random()` funktionen, både i længderne af firekanterne men også farverne. Dermed skabes lidt generativitet og ukontrollerbarhed i programmet. Jeg får lidt lyst til at lave et par printede versioner af min kode, måske fordi jeg egenlig godt kan lide de randomme farver - og selvfølgelig fordi jeg er stolt af min første kode! :D

# En interessant oplevelse
Da jeg lige startede med at bede programmet om at tegne firekanter, gjorde den det i et vild hurtig tempo, så man fik næsten epilepsi af det. Så for at gøre koden langsommere, satte jeg framerate lavere inde i setup funktionen:

```
function setup() {
  
  createCanvas(500, 500); //lav et canvas på 500x500 pixels
  background(255, 220, 220); //gør baggrunden for canvas lyderød
  frameRate(5); //sætter et lavere framerate, så man ikke får et epileptisk anfald :P

}
```
På den måde kan man rent faktisk se progessionen i at firekanterne bliver genereret og tegnet. 

Jeg elsker forresten at lave **kommenteringer**, det gør programmering lidt mere socialt på en måde, fordi jeg ved at nogle andre (mennesker) kommer til at læse dem for at forstå min kode bedre.

# Refleksioner omkring programmet
Jeg gættede mig ret meget frem til farverne - altså jeg testede bare en masse tal af, og så hvordan det så ud på canvasset. Det er interessant hvordan maskinen læser farver som RGB værdier, hvilket betyder ingenting for mig. Når vi maler et billede _In Real Life_ kan jeg hele tiden se malingens farve og nemt blande det med andre farver. Men fordi maskinen snakker et andet sprog, er jeg nødt til at bruge flere tankekræfter på at komme frem til den farve, jeg gerne vil have: Hvis farven skal være lysere, skal værdierne generelt være højere. Hvis farven skal være mere pang, skal forskellen i værdierne være størrer osv.

At programmere gør, at jeg bliver nødt til at tænker anderledes. Jeg skal tænke ligesom maskinen. Det har måske lidt med det, der står i grundbogen at gøre:

> … programming as a means to think differently

Vi begynder at forstå maskinens måde at arbejde på, og respondere på det. Vi bliver på en måde suget ind i et andet verdens-modus. Derfra kan vi så se på vores nuværende verden på en ny måde. For eksempel det med farveværdier:
- Kan hele verdenens farver reduceres til kun 3 tal?
- Hvad med alle de farver, som menneskeøjet ikke kan se, men som dyr og planter kan se? Kan vi lave computer skærme, som hunde fx synes er flotte at se på?

Derudover synes jeg det er sjovt at tænke på, at skærmen "snyder" vores sanser ved at køre i 60fps, så det ser ud som om at ting på skærmen bevæger sig, men faktisk er det bare lynhurtige stilbilleder, som opdateres konstant. Det minder mig om hvordan man (i hvert fald i gamle dage) når man prøvede at optage en video af skærmen, så flimrede det helt vildt. Det er jo nok pga. forskellen i framrate? 

Alt dette giver mig yderligere blod på tanden ift. at lære hvad der ellers forgår "bag" computeren. Der er rigtig mange ting ved computeren, som nogle andre folk har opfundet og bestemt sig for. Disse folk er endda højt sandsynlig hvide mænd i silicon vally. Gad vide hvordan computeren vil være, hvis det kun var kvinder, som designede og programmede dem? På samme tid vil jeg heller ikke virke kønsnormativ... Men måske mangler der noget feminin touch i nutidens computere? Hvad end det betyder :P

# Opgraderingsmuligheder
Det kunne være nice, hvis firekanterne starter på random x værdier også. Og jeg gad også godt have en knap, så man kan downloade et screenshot af sit billede. Måske kunne programmet også bruges til at generere en random farvepalette til en? Men så bliver det måske for "programmering til at løse et problem"-agtigt. Jeg kan godt lide at den er lidt meningsløs, og at programmet bare er en udforskelsesprocess.

# Referenceliste
- Lorem Ipsum
- Lorem Ipsum
- Lorem Ipsum
