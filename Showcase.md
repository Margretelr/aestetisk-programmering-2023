# MiniX[1]: RunMe og ReadMe (individuel) 
### Work by Amalie Doktor Skødt Simonsen

https://gitlab.com/AmalieDoktor/aestetiskprogrammering/-/tree/main/MiniX1

![](https://user-content.gitlab-static.net/33f804d16db102955c173d2bac4525a805d34b98/68747470733a2f2f616d616c6965646f6b746f722e6769746c61622e696f2f61657374657469736b70726f6772616d6d6572696e672f2f4d696e6958312f536b254333254136726d62696c6c6564655f323032332d30322d31315f3135333831302e706e67)

### Work by Ann-Katrine Bidstrup Sørensen

https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/tree/main/MiniX%201

![](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/raw/main/MiniX%201/Screenshot_.png)

# MiniX[2]: Geometrisk emoji (individuel) 
### Work by Carl Mathias Vestbo Johansen

https://gitlab.com/carlvestbo/aestetiskprogrammering/-/tree/main/MiniX2

![](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/raw/main/MiniX2/earthmoji.png)
 
### Work by Salomon Frederik K H Simonsen

https://gitlab.com/salomon7695206/au-programmering/-/tree/main/MiniX2
![](https://gitlab.com/salomon7695206/au-programmering/-/raw/main/MiniX2/Screenshot_2023-02-19_161011.png)
 
# MiniX[3]: Design en throbber (individuel) 
### Work by Gustav Fridthiof Engbo

https://gitlab.com/gustavfe/aestetiskprogrammering/-/tree/main/miniX3

 ![](https://gitlab.com/gustavfe/aestetiskprogrammering/-/raw/main/miniX3/Screen_Shot_2023-03-03_at_15.30.51.png)

### Work by Ann-Katrine Bidstrup Sørensen
https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/tree/main/MiniX%203 
 
![](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/raw/main/MiniX%203/screenshot3.png)

# MiniX[4]: Capture All (individuel) 
### Spor by Thea Heskjær

https://gitlab.com/thea.heskjaer/aestetisk-programmering/-/tree/main/miniX4 

 ![](https://gitlab.com/thea.heskjaer/aestetisk-programmering/-/raw/main/miniX4/minix43.png)

### The dark side by Joan Gunnarsdóttir Hansen
https://gitlab.com/joan6375305/aestetisk-programmering/-/tree/main/MiniX4

![](https://gitlab.com/joan6375305/aestetisk-programmering/-/raw/main/MiniX4/hvid.png)
 
# MiniX[5]: A Generative Program (individuel) 
### Work by Laura Kirstine Møller Jensen
https://gitlab.com/laurakmj1512/aep/-/tree/main/Minix5 

 ![](https://gitlab.com/laurakmj1512/aep/-/raw/main/Minix5/Sk%C3%A6rmbillede_2023-03-19_kl._20.53.53.png)

### Scrabble by Sif Wilja Steenfeldt
https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/tree/main/minix5

![](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/raw/main/minix5/Sk%C3%A6rmbillede_2023-03-15_194118.png)
 
# MiniX[6]: Games with objects (individuel) 
### Bottle Collector by Marta Tausen

https://gitlab.com/martatausen/aesthetic-programmering/-/tree/main/minix6

![](https://gitlab.com/martatausen/aesthetic-programmering/-/raw/main/minix6/screen1.png)
 
### Asteroid by Mathias Sund Sørensen

https://gitlab.com/MathiasSund/aestetisk-programmering/-/tree/main/MiniX6

 ![](https://gitlab.com/MathiasSund/aestetisk-programmering/-/raw/main/MiniX6/minix6billede.png)
# MiniX[8]: Flowcharts (individuel + gruppe) 
Flowchart by Kevin Klyssing Jensen
https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/tree/main/MiniX_8

 ![](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/raw/main/MiniX_8/Flowchartminix8.jpg)
# MiniX[9]: E-lit (gruppe) 
### Artificial Sentiment by Sif Wilja Steenfeldt, Ann-Katrine Bidstrup Sørensen, Gustav Fridthiof Engbo

https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/tree/main/MiniX9

 ![](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/raw/main/MiniX9/skarm.png)

### In my head by Thea Lukassen Heskjær, Thea Maria Uhd, Kathrine Morgenstjerne Hansen, Camilla Boesen, Rikke Osmann Jessen
https://gitlab.com/cami601k/aestetiskprogrammering/-/tree/main/MiniX9

 ![](https://gitlab.com/cami601k/aestetiskprogrammering/-/raw/main/MiniX9/minix9.gif)

# MiniX[10]: Machine unlearning (gruppe) 

### Sentiment Analysis by Laura Kirstine Møller Jensen, Lisa Birungi Andersen, Elma Erkocevic
https://gitlab.com/lisabirungi/privataestetiskprogrammering/-/tree/main/miniX_gruppe10/minix10

 ![](https://gitlab.com/lisabirungi/privataestetiskprogrammering/-/raw/main/miniX_gruppe10/minix10/Sentimentscore_screenshot.png)

### ImageClassifier by Maise Lesmann Alling, Mads Leth Hansen, Job Silas Nisgaard Ehrich og Sofie-Amalie Møller
https://gitlab.com/sofieamaliem/aestetisk-programmering/-/tree/main/MiniX10 

![](https://gitlab.com/sofieamaliem/aestetisk-programmering/-/raw/main/MiniX10/Sk%C3%A6rmbillede3.png)

# MiniX[11]: Endelige projekt (gruppe) 
TBA
 
